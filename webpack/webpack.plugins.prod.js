const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const basePlugins = require('./webpack.plugins.base.js');
const path = require('path');

module.exports = [
  ...basePlugins,
  new CleanWebpackPlugin(['dist'], {
    root: path.join(__dirname, '..'),
    verbose: false,
  }),
  new CopyWebpackPlugin([
    {
      from: '../../public',
      to: 'public',
    },
  ]),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      HOST: JSON.stringify(process.env.HOST),
      PORT: JSON.stringify(process.env.PORT),
    },
  }),
  new webpack.LoaderOptionsPlugin({
    minimize: true,
    debug: false,
  }),
  new webpack.optimize.UglifyJsPlugin({
    beautify: false,
    mangle: {
      screw_ie8: true,
      keep_fnames: true,
    },
    compress: {
      warnings: false,
      screw_ie8: true,
    },
    comments: false,
  }),
  new webpack.NamedModulesPlugin(),
  new ProgressBarPlugin(),
];
