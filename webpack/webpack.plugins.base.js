const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineManifestWebpackPlugin = require('inline-manifest-webpack-plugin');
const isProduction = process.env.NODE_ENV === 'production';

module.exports = [
  new HtmlWebpackPlugin({
    template: 'index.ejs',
    templateParameters: {
      isProduction
    }
  }),
  new InlineManifestWebpackPlugin({
    name: 'webpackManifest',
  }),
];
