const webpack = require('webpack');
const basePlugins = require('./webpack.plugins.base.js');
const WriteFilePlugin = require('write-file-webpack-plugin');
const config = require('../config');

module.exports = [
  ...basePlugins,
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
  new WriteFilePlugin(),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('development'),
      HOST: JSON.stringify(config.serverHost),
      PORT: JSON.stringify(config.clientPort),
      SERVER_API: JSON.stringify(`http://${config.serverHost}:${config.serverApiPort}`),
    },
  }),
];
