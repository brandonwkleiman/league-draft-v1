const path = require('path');
const webpackModule = require('./webpack.module');

const isProduction = process.env.NODE_ENV === 'production';
const filename = '[name].js';

const host = process.env.HOST || '127.0.0.1';
const port = +process.env.PORT || 3000;

if (!isProduction) {
  console.log(`\nWebpack Dev Server running on [${host}:${port}]`); // eslint-disable-line no-console
}

module.exports = {
  context: path.join(__dirname, '..', 'src', 'client'),
  entry: {
    app: './index.js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  node: {
    fs: 'empty',
  },
  output: {
    filename,
    path: path.join(__dirname, '..', '/dist'),
    publicPath: '/',
  },
  module: webpackModule,
  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, '..', '/'),
    host,
    port,
    hot: true,
    hotOnly: true,
    inline: true,
    stats: {
      colors: true,
    },
    watchOptions: {
      poll: 1000,
    },
    public: `${host}:${port}`,
  },
};
