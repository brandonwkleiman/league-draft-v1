const clc = require('cli-color');

const environmentConfig = require('./envConfig.json');

const warnIcon = clc.yellow(String.fromCharCode(9888));
const checkIcon = clc.green(String.fromCharCode(10004));
const errorIcon = clc.red(String.fromCharCode(10008));

const getConfig = () => {
  const { NODE_ENV = 'development' } = process.env;

  if (typeof window !== 'undefined') {
    throw new Error("Config shouldn't be called by the client.");
  }

  const log = console.log; // eslint-disable-line no-console

  const envObject = {};

  log('\n');
  log(clc.black.bgWhite(`\n Setting up environment variables for ${NODE_ENV}:\n`));
  log('\n');

  environmentConfig.forEach((env) => {
    const { key, id } = env;

    if (process.env[key]) {
      envObject[id] = process.env[key];
      log(`${checkIcon} ${clc.bold(env.key)}`);
    } else if (env.default && env.default[NODE_ENV]) {
      log(`${warnIcon} Using default for ${clc.bold(env.key)} value ${env.default[NODE_ENV]}`);
      envObject[id] = env.default[NODE_ENV];
    } else {
      if (NODE_ENV === 'production' && environmentConfig.devOnly) {
        return;
      }

      log(`${errorIcon} Environment variable not found: ${clc.red.bold(env.key)}`);
    }
  });

  return envObject;
};

module.exports = getConfig();
