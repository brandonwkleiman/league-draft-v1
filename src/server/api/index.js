const express = require('express');
const asyncHandler = require('express-async-handler');

const userApi = require('./users');
const gameApi = require('./games');

const router = express.Router();
router.use('/users', userApi);
router.use('/games', gameApi);

module.exports = router;
