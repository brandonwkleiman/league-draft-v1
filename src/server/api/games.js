const express = require('express');
const asyncHandler = require('express-async-handler');

const GameLib = require('../lib/games');

const router = express.Router();

router.get(
  '/:gameId',
  asyncHandler(async (req, res) => {
    const { gameId } = req.params;
    if (!gameId) {
      throw new Error();
    }
    try {
      const data = await GameLib.findByURL(gameId);
      return res.send({
        data
      });
    } catch (e) {
      if (e instanceof Error) {
        throw new Error();
      }
    }
    throw new Error();
  })
);

module.exports = router;
