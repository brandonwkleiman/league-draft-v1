const express = require('express');
const asyncHandler = require('express-async-handler');

const UserLib = require('../lib/users');

const router = express.Router();

router.get(
  '/:userId',
  asyncHandler(async (req, res) => {
    const { userId } = req.params;
    if (!userId) {
      throw new Error();
    }
    try {
      const data = await UserLib.findById(userId);
      return res.send({
        data
      });
    } catch (e) {
      if (e instanceof Error) {
        throw new Error();
      }
    }
    throw new Error();
  })
);

module.exports = router;
