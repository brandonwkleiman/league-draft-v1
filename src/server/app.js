const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const path = require('path');

const config = require('../../config');
const api = require('./api');

const app = express();

app.use(
  morgan(
    ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'
  )
);
app.use(bodyParser.json());
app.use(compression());
app.use(cors());

app.use(express.static(path.join(__dirname, '..', '..', '/dist')));
app.use('/api', api);

app.use('*', (req, res) =>
  res.sendFile(path.join(__dirname, '..', '..', '/dist', 'index.html'))
);

module.exports = app;
