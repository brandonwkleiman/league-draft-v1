const moment = require('moment');
const database = require('../db');
const Sequelize = require('sequelize');

const UserSchema = database.define('User', {
  username: Sequelize.STRING,
  avatar_id: Sequelize.STRING,
  password: Sequelize.STRING,
  online_status: Sequelize.BOOLEAN
});

UserSchema.getterMethods = {
  findById() {
    return this.username;
  }
};

UserSchema.sync();

module.exports = UserSchema;
