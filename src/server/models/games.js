const moment = require('moment');
const database = require('../db');
const Sequelize = require('sequelize');

const GameSchema = database.define('Game', {
  url: Sequelize.STRING,
  is_active: Sequelize.BOOLEAN
});

GameSchema.sync();

module.exports = GameSchema;
