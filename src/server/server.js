const clc = require('cli-color');
const db = require('./db');
const config = require('../../config');
const apiServer = require('./apiServer');

const host = config.serverHost;
const port = +config.serverPort;

const log = console.log;

const environment = config.environment;
const warn = clc.yellow;
const warnIcon = String.fromCharCode(9888);
const info = clc.blue;
const infoIcon = String.fromCharCode(9873);

const startServer = () => {
  switch (environment) {
    case 'production':
      log('\n');
      log(clc.white.bgRed('\n Server running in production mode.\n'));
      log('\n');
      log(
        warn(warnIcon),
        `Make sure you have built the app with ${warn(
          'yarn build'
        )} before running this command.`
      );
      log(
        info(infoIcon),
        `If you want to use development, type ${info(
          'yarn start:dev'
        )} instead.`
      );

      apiServer(host, port);
      break;
    default:
      log('\n');
      log(clc.white.bgGreen('\n Server running in development mode.\n'));
      log('\n');
      apiServer(host, port);
      break;
  }
};

db.authenticate()
  .then(() => {
    startServer();
  })
  .catch(err => {
    log(clc.white.bgRed('\n Unable to connect to database.\n'));
    log(err);
  });
