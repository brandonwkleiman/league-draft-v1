const app = require('./app');
const sockets = require('./sockets');

module.exports = (host, port) => {
  app.listen(port, host);
};
