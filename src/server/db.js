const Sequelize = require('sequelize');

const config = require('../../config');

const dbHost = config.dbHost;
const database = config.database;
const dbUser = config.dbUser;
const dbPass = config.dbPass;

const sequelize = new Sequelize(database, dbUser, dbPass, {
  host: dbHost,
  dialect: 'postgres',
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

module.exports = sequelize;
