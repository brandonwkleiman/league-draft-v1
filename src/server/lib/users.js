const moment = require('moment');
const UserModel = require('../models/users');

class UsersLib {
  static async findById(id) {
    if (!id) {
      throw new Error();
    }
    const product = await UserModel.findById(id).catch(() => {
      throw new Error();
    });
    return product;
  }
}

module.exports = UsersLib;
