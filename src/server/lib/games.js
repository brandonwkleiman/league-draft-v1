const moment = require('moment');
const GameModel = require('../models/games');

class GamesLib {
  static async findByURL(url) {
    if (!url) {
      throw new Error();
    }
    const product = await GameModel.find({
      where: {
        url
      }
    })
      .then(resp => resp)
      .catch(() => {
        throw new Error();
      });
    return product;
  }
  async queryByURL() {
    return;
  }
}

module.exports = GamesLib;
