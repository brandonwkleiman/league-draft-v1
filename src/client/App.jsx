import React from 'react';
import { hot } from 'react-hot-loader/root';
import { createAppStore } from './lib/store';

import './assets/reset.css'; // eslint-disable-line import/no-unresolved
import './assets/scss/bootstrap.scss';
import './base.scss';

import Root from './Root';

const store = createAppStore();

const App = () => <Root store={store} />;
export default hot(App);
