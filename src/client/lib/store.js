import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import reducers from './state';

export const createAppStore = () =>
  createStore(reducers, applyMiddleware(thunk, logger));
