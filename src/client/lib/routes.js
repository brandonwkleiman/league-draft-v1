import React from 'react';
import { Router, Redirect } from '@reach/router';
import Landing from '../containers/Landing';
import Create from '../containers/Create';

export const Routes = props => (
  <Router>
    <Landing {...props} path="/" />
    <Create {...props} path="/create" />
    <Redirect noThrow default to="/" />
  </Router>
);

export default Routes;
