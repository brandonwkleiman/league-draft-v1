import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { createHistory, LocationProvider } from '@reach/router';

import theme from './lib/theme';
import Home from './views/Home/Home';

let history = createHistory(window);

export default function Root({ store }) {
  return (
    <Provider store={store}>
      <LocationProvider history={history}>
        <ThemeProvider theme={theme}>
          <Home />
        </ThemeProvider>
      </LocationProvider>
    </Provider>
  );
}

Root.propTypes = {
  store: PropTypes.object.isRequired
};
