import styled, { keyframes } from 'styled-components';

export const HomeContainer = styled.div`
  height: 100vh;
  min-height: 500px;
  display: grid;
  grid-template-columns: 1fr 200px;
  grid-template-rows: ${({ theme }) => theme.vars.navHeight} 1fr;

  .nav {
    grid-row: 1;
    grid-column: 1/3;
  }
  .main {
    background-size: 100em 100%;
    background-image: -webkit-linear-gradient(
      left,
      #031426 0%,
      #001b3a 25%,
      #031426 50%,
      #031426 50.1%,
      #001b3a 75%,
      #031426 100%
    );
    animation: Animation 15s linear infinite;

    grid-row: 1/3;
    grid-column: 1;
  }

  .friends {
    grid-row: 1/3;
    grid-column: 2;
  }

  @keyframes Animation {
    0% {
      background-position: 0 0;
    }
    25% {
      background-position: -25em 0;
    }
    50% {
      background-position: -50em 0;
    }
    75% {
      background-position: -75em 0;
    }
    100% {
      background-position: -99em 0;
    }
  }
`;
export const LoadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  position: relative;
  > * {
    position: absolute;
    left: calc(50% - 1em);
    top: calc(50% - 1em);
  }
`;

export const LandingContent = styled.div`
  padding-top: ${({ theme }) => theme.vars.navHeight};
  color: ${({ theme }) => theme.colors.gold};
  font-weight: 400;
  h1 {
    margin-top: 40px;
    text-align: center;
  }
`;
