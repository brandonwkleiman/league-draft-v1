import types from '../../lib/constants';

const initialState = {
  loading: false
};

const HomeReducers = (state = initialState, action) => {
  switch (action.type) {
    case types.HOME_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
};

export default HomeReducers;
