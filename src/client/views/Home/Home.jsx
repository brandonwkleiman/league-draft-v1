import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { HomeContainer, LandingContent, LoadingContainer } from './styles';
import { updateMainButton as updateMainButtonAction } from '../../containers/Nav/actions';
import { initNewGame as initNewGameAction } from './actions';
import { Spinner } from '../../components';
import Friends from '../../containers/Friends';
import Nav from '../../containers/Nav';
import Routes from '../../lib/routes';
import history from '../../lib/history';

class Home extends Component {
  constructor() {
    super();
  }

  render() {
    const { loading } = this.props;

    return (
      <HomeContainer>
        <Friends history={history} className="friends" />
        <LandingContent className="main">
          {loading ? (
            <LoadingContainer>
              <Spinner />
            </LoadingContainer>
          ) : (
            <Routes {...this.props} />
          )}
        </LandingContent>
        <Nav {...this.props} history={history} className="nav" />
      </HomeContainer>
    );
  }
}

Home.propTypes = {
  loading: PropTypes.bool
};

Home.defaultProps = {
  loading: false
};

const mapStateToProps = state => ({
  loading: state.home.loading
});

const mapDispatchToProps = {
  updateMainButton: updateMainButtonAction,
  initNewGame: initNewGameAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
