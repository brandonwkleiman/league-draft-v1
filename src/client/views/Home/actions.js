import openSocket from 'socket.io-client';
import axios from 'axios';
import types from '../../lib/constants';
const socket = openSocket('http://localhost:8080');

export function initNewGame(type) {
  return dispatch => {
    dispatch({
      type: types.HOME_LOADING,
      payload: true
    });
    
    const url = 'http://localhost:3000/api/games';
    const data = { type: 'public' };

    axios({
      method: 'POST',
      url,
      headers: {
        'Content-Type': 'application/json'
      },
      data
    })
    .then(resp => {
      console.log(resp.data.id)
      socket.emit('connection', 1000);
      socket.emit('status', resp.data.id);
    })
    .then(err => {
      console.log(err)
    })
  }
}
