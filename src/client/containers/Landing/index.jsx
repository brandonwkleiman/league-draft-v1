import React, { useEffect } from 'react';
import styled from 'styled-components';

const Main = props => {
  useEffect(() => {
    props.updateMainButton('PLAY', '/create', true);
  }, []);
  return <h1>Main</h1>;
};
export default Main;
