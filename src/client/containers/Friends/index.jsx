import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container } from './styles';

class Friends extends Component {
  constructor() {
    super();
  }

  render() {
    const { className } = this.props;

    return <Container className={className} />;
  }
}

Friends.propTypes = {
  loading: PropTypes.bool
};

Friends.defaultProps = {
  loading: false
};

const mapStateToProps = state => ({
  loading: state.friends.loading
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Friends);
