import types from '../../lib/constants';

const initialState = {
  loading: false
};

const FreindsReducers = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default FreindsReducers;
