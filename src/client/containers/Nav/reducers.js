import types from '../../lib/constants';

const initialState = {
  loading: false,
  buttonText: 'PLAY',
  buttonLink: '/create'
};

const NavReducers = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
    case types.UPDATE_BUTTON:
      return {
        ...state,
        buttonText: action.payload.label,
        buttonLink: action.payload.location,
        isActive: action.payload.isActive
      };
  }
};

export default NavReducers;
