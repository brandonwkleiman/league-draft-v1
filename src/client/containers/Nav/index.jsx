import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container } from './styles';
import { PrimaryButton } from '../../components';

class Nav extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { className, buttonText, buttonLink } = this.props;

    return (
      <Container className={className}>
        <PrimaryButton to={buttonLink}>{buttonText}</PrimaryButton>
      </Container>
    );
  }
}

Nav.propTypes = {
  loading: PropTypes.bool,
  buttonText: PropTypes.string,
  buttonLink: PropTypes.string
};

Nav.defaultProps = {
  loading: false,
  buttonText: 'PLAY',
  buttonLink: '/create'
};

const mapStateToProps = state => ({
  loading: state.nav.loading,
  buttonText: state.nav.buttonText,
  buttonLink: state.nav.buttonLink
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);
