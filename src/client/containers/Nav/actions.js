import types from '../../lib/constants';

export function updateMainButton(
  label = 'PLAY',
  location = '/create',
  isActive = true
) {
  return {
    type: types.UPDATE_BUTTON,
    payload: {
      label,
      location,
      isActive
    }
  };
}
