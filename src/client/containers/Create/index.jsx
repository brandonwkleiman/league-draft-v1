import React, { useState, useEffect } from 'react';
import LockIcon from '../../assets/images/LockIcon';
import PublicIcon from '../../assets/images/PublicIcon';
import { GameType, PrimaryButton } from '../../components';
import { Modes } from './styles';

const gameModes = [
  {
    name: 'public',
    icon: PublicIcon,
    description: 'Visible to everyone',
    url: '/sessions/public'
  },
  {
    name: 'private',
    icon: LockIcon,
    description: 'Visible only to your friends or by direct URL',
    url: '/sessions/private'
  }
];
const Create = props => {
  const [activeMode, selectMode] = useState();
  useEffect(() => {
    props.updateMainButton('HOME', '/', true);
  }, []);

  return (
    <div>
      <Modes>
        {gameModes.map((mode, idx) => (
          <GameType
            active={activeMode === mode.name}
            key={mode.name}
            handleSelect={selectMode}
            {...mode}
          />
        ))}
      </Modes>
      <div className="d-flex justify-content-center">
        {activeMode && (
          <PrimaryButton
            type="link"
            onClick={() => props.initNewGame(activeMode)}
          >
            Continue
          </PrimaryButton>
        )}
      </div>
    </div>
  );
};
export default Create;
