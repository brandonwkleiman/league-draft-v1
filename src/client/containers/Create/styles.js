import React from 'react';
import styled from 'styled-components';

export const Modes = styled.div`
  display: flex;
  justify-content: space-evenly;
  padding: 60px;
  .mode-option {
    text-align: center;
    opacity: 0.5;
    cursor: pointer;
    .details {
      opacity: 0;
    }
    &.active,
    &:hover {
      opacity: 1;
    }
    &.active .details {
      opacity: 1;
    }
  }
  svg {
    height: 80px;
    width: 80px;
    * {
      fill: ${({ theme }) => theme.colors.gold};
    }
  }
`;
