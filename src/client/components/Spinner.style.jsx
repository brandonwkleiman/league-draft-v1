import React from 'react';
import styled from 'styled-components';

export const Spinner = styled(({ className }) => (
  <i className={`fas fa-sync fa-spin fa-4x ${className}`} />
))`
  margin: auto;
  .fa,
  .fas {
    font-family: 'Font Awesome\ 5 Free';
  }
`;
