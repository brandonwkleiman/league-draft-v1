import React from 'react';
import styled from 'styled-components';

const GameTypeWrapper = styled.div`
  flex: 1 1 0;
  p {
    font-weight: 700;
    width: 50%;
    margin: 0 auto;
    line-height: 19px;
    font-size: 14px;
  }
`;

export const GameType = ({ ...props }) => {
  const className = props.active ? 'mode-option active' : 'mode-option';
  return (
    <GameTypeWrapper
      onClick={() => props.handleSelect(props.name)}
      className={className}
    >
      {props.icon()}
      <div className="details">
        <h1>{props.name}</h1>
        <p>{props.description}</p>
      </div>
    </GameTypeWrapper>
  );
};
