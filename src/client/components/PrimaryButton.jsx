import React from 'react';
import styled, { css } from 'styled-components';
import { Link } from '@reach/router';

const styles = css`
  display: inline-block;
  height: 25px;
  margin: 18px;
  min-width: 50px;
  text-align: center;
  padding: 0 40px;
  background: #00152c;
  line-height: 25px;
  color: #cdbd93;
  font-size: 14px;
  border-radius: 8px;
  border: 2px solid #0c5c54;
  font-weight: 500;
  transition: border-color 0.3s ease;
  text-decoration: none;
  &:hover {
    border-color: turquoise;
    color: gold;
    transition: border-color 0.1s ease;
    cursor: pointer;
  }
`;
const StyledButton = styled.a`
  ${styles}
`;
const StyledLink = styled(Link)`
  ${styles}
`;

export const PrimaryButton = ({ ...props }) => {
  if (props.type === 'link') {
    return <StyledButton {...props}>{props.children}</StyledButton>;
  }
  return (
    <StyledLink to={props.to} {...props}>
      {props.children}
    </StyledLink>
  );
};
