# Draft Draft

## Getting Started
```
git clone https://brandonwkleiman@bitbucket.org/brandonwkleiman/league-draft-v1.git
cd league-draft-v1
yarn install
```

## Development
### Running the frontend
```
yarn start:dev:client
```
### Starting the Server
```
yarn start:dev:server
```

## Linting and Formatting
```
yarn lint:fix
yarn format
```
